import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';

import indexRouter from './routes/index';
import doorRouter from './routes/door';

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.use('/', indexRouter);
app.use('/door', doorRouter);

app.listen(process.env.PORT, () => {
  console.log(`Express with Typescript! http://localhost:${process.env.PORT}`);
});
