import express from 'express';
import { Deta } from 'deta';
import moment from 'moment-timezone';
import randomString from '../utils/random';

const router = express.Router();

const deta = Deta(process.env.AMNESIASAVIOR_DB_KEY);
const doorDB = deta.Base('door');
const doorLogDB = deta.Base('door-log');

/* POST set house key. */
router.post('/house', async (req, res) => {
  try {
    const hasMasterKey = () => {
      return !!req.body.masterKey;
    };
    const isMasterKey = () => {
      return hasMasterKey() && req.body.masterKey === process.env.MASTER_KEY_HOUSE;
    };
    const keyStr = isMasterKey() ? req.body.masterKey : randomString();
    const ts = moment().valueOf();
    await doorDB.put(keyStr, 'house');
    await doorLogDB.put(
      {
        door: 'house',
        doorKey: keyStr,
        date: moment(ts).tz('Asia/Taipei').format('YYYY-MM-DD HH:mm:ss'),
        reason: isMasterKey()
          ? '🔑 Master key!'
          : hasMasterKey()
          ? '🚫 Error master key!'
          : req.body.reason,
      } as any,
      String(ts),
    );
    res.send('POST request to the house');
  } catch (error) {
    res.send('POST /house error');
  }
});

/* GET get house key. */
router.get('/house', async (req, res, next) => {
  try {
    const keyObj = await doorDB.get('house');
    res.send(keyObj);
  } catch (error) {
    res.send('GET /house error');
  }
});

/* POST set apartment key. */
router.post('/apartment', async (req, res) => {
  try {
    const hasMasterKey = () => {
      return !!req.body.masterKey;
    };
    const isMasterKey = () => {
      return hasMasterKey() && req.body.masterKey === process.env.MASTER_KEY_APARTMENT;
    };
    const keyStr = isMasterKey() ? req.body.masterKey : randomString();
    const ts = moment().valueOf();
    await doorDB.put(keyStr, 'apartment');
    await doorLogDB.put(
      {
        door: 'apartment',
        doorKey: keyStr,
        date: moment(ts).tz('Asia/Taipei').format('YYYY-MM-DD HH:mm:ss'),
        reason: isMasterKey()
          ? '🔑 Master key!'
          : hasMasterKey()
          ? '🚫 Error master key!'
          : req.body.reason,
      } as any,
      String(ts),
    );
    res.send('POST request to the apartment');
  } catch (error) {
    res.send('POST /apartment error');
  }
});

/* GET get apartment key. */
router.get('/apartment', async (req, res, next) => {
  try {
    const keyObj = await doorDB.get('apartment');
    res.send(keyObj);
  } catch (error) {
    res.send('GET /apartment error');
  }
});

/* GET get door log. */
router.get('/log', async (req, res, next) => {
  try {
    const keyObj = await doorLogDB.fetch();
    res.send(keyObj);
  } catch (error) {
    res.send('GET /apartment error');
  }
});

export default router;
