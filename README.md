## Getting Started

```bash
# watch
yarn dev

# build & run in production
yarn prod

# build in space dev
space push
```

## Ref.

[後端架設使用 Deta Space](https://deta.space/docs/en/introduction/start)

[在 Node 和 Express 中使用 ES6 （及以上）语法](https://www.freecodecamp.org/chinese/news/how-to-enable-es6-and-beyond-syntax-with-node-and-express/)

[教你如何使用 Node + Express + Typescript 开发一个应用](https://toutiao.io/posts/6yxdar1/preview)
